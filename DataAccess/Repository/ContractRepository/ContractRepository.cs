﻿using Data;
using Domain.Entities;

namespace DataAccess.Repository.ContractRepository
{
    public class ContractRepository : Repository<Contract>, IContractRepository, IRepositoryIdentifier
    {
        public ContractRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
