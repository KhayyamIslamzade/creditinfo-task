﻿using Domain.Entities;

namespace DataAccess.Repository.ContractRepository
{
    public interface IContractRepository : IRepository<Contract>
    {
    }
}
