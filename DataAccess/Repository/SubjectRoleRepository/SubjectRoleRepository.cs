﻿using Data;
using Domain.Entities;

namespace DataAccess.Repository.SubjectRoleRepository
{
    public class SubjectRoleRepository : Repository<SubjectRole>, ISubjectRoleRepository, IRepositoryIdentifier
    {
        public SubjectRoleRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
