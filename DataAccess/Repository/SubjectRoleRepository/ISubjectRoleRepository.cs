﻿using Domain.Entities;

namespace DataAccess.Repository.SubjectRoleRepository
{
    public interface ISubjectRoleRepository : IRepository<SubjectRole>
    {
    }
}
