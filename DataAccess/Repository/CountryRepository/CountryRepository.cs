﻿using Data;
using Domain.Entities;

namespace DataAccess.Repository.CountryRepository
{
    public class CountryRepository : Repository<Country>, ICountryRepository, IRepositoryIdentifier
    {
        public CountryRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
