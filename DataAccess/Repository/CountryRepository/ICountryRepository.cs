﻿using Domain.Entities;

namespace DataAccess.Repository.CountryRepository
{
    public interface ICountryRepository : IRepository<Country>
    {
    }
}
