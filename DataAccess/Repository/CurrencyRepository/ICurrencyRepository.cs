﻿using Domain.Entities;

namespace DataAccess.Repository.CurrencyRepository
{
    public interface ICurrencyRepository : IRepository<Currency>
    {
    }
}
