﻿using Data;
using Domain.Entities;

namespace DataAccess.Repository.CurrencyRepository
{
    public class CurrencyRepository : Repository<Currency>, ICurrencyRepository, IRepositoryIdentifier
    {
        public CurrencyRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
