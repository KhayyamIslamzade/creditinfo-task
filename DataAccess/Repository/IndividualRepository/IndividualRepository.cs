﻿using Data;
using Domain.Entities;

namespace DataAccess.Repository.IndividualRepository
{
    public class IndividualRepository : Repository<Individual>, IIndividualRepository, IRepositoryIdentifier
    {
        public IndividualRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
