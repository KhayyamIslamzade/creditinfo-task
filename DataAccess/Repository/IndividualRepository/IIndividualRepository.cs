﻿using Domain.Entities;

namespace DataAccess.Repository.IndividualRepository
{
    public interface IIndividualRepository : IRepository<Individual>
    {
    }
}
