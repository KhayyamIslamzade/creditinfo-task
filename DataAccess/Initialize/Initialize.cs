﻿using System.Linq;
using System.Threading.Tasks;
using Data;
using DataAccess.Initialize.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Initialize
{
    public static class Initialize
    {
        public static async Task SeedAsync(ApplicationDbContext context)
        {
            await context.Database.MigrateAsync();
            await context.Database.EnsureCreatedAsync();

            await SeedCurrencyAsync(context).ConfigureAwait(false);
            await SeedCountryAsync(context).ConfigureAwait(false);



        }

        private static async Task SeedCurrencyAsync(ApplicationDbContext context)
        {
            var data = InitializeData.BuildCurrencyList();
            var dbData = context.Currencies.ToList();
            var diffData = data.Where(c => dbData.All(e => e.Code != c.Code)).ToList();
            await context.Currencies.AddRangeAsync(diffData).ConfigureAwait(false);
            await context.SaveChangesAsync().ConfigureAwait(false);
        }
        private static async Task SeedCountryAsync(ApplicationDbContext context)
        {
            var data = InitializeData.BuildCountryList();
            var dbData = context.Countries.ToList();
            var diffData = data.Where(c => dbData.All(e => e.Name != c.Name)).ToList();
            await context.Countries.AddRangeAsync(diffData).ConfigureAwait(false);
            await context.SaveChangesAsync().ConfigureAwait(false);
        }


    }
}
