﻿using System.Collections.Generic;
using Domain.Entities;

namespace DataAccess.Initialize.Data
{
    public static partial class InitializeData
    {
        public static List<Country> BuildCountryList()
        {
            var list = new List<Country>()
            {
                new ("Azerbaijan"),
                new ("Ukraine"),
                new ("America"),
            };

            return list;
        }
    }
}
