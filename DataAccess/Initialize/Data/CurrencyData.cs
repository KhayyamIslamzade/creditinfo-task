﻿using System.Collections.Generic;
using Domain.Entities;

namespace DataAccess.Initialize.Data
{
    public static partial class InitializeData
    {
        public static List<Currency> BuildCurrencyList()
        {
            var list = new List<Currency>()
            {
                new ("AZN"),
                new ("UAH"),
                new ("USD"),
            };

            return list;
        }
    }
}
