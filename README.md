# How To Run?
For the run the project you just need to **uncomment line 165** in DependencyInjection.cs and **comment line 162** (because in my computer i dont have MS SQL Server and it will be take a lot of time to install) and type "add-migration init" (you should select "Data" as default project) and "dotnet run"
_Application will seed 3 Country and 3 Currency after run._


# Completed Areas
-  DAL+DB design
-  Search API 
-  little bit about XSD validation(it's in TestController)

**Swagger Url : /api/swagger**



