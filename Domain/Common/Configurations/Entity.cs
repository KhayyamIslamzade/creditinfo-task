﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Enums;

namespace Domain.Common.Configurations
{
    public class Entity : IEntity
    {
        public RecordStatusEnum Status { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public DateTime? DateDeleted { get; set; }

        public Entity()
        {
            Status = RecordStatusEnum.Active;
        }
    }
}
