﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Common.Configurations;

namespace Domain.Entities
{
    public class Country : Entity
    {
        public Country()
        {
        }
        public Country(string name)
        {
            Name = name;
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
