﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Common.Configurations;

namespace Domain.Entities
{
    public class Contract : Entity
    {
        [Key]
        [Required]
        public string ContractCode { get; set; }
        [Column(TypeName = "date")]
        public DateTime DateOfLastPayment { get; set; }
        [Column(TypeName = "date")]
        public DateTime NextPaymentDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime DateAccountOpened { get; set; }

        [Required]
        [StringLength(1024)]
        public string PhaseOfContract { get; set; }


        [ForeignKey("OriginalContract")]
        public int OriginalAmountId { get; set; }
        public Amount OriginalAmount { get; set; }


        [ForeignKey("InstallmentAmount")]
        public int InstallmentAmountId { get; set; }
        public Amount InstallmentAmount { get; set; }


    }
}
