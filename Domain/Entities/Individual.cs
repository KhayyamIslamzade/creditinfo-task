﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Common.Configurations;
using Domain.Common.Enums;

namespace Domain.Entities
{
    public class Individual : Entity
    {
        [Key]
        [Required]
        public string CustomerCode { get; set; }
        [Required]
        [StringLength(128)]
        public string Surname { get; set; }
        [Required]
        [StringLength(128)]
        public string Lastname { get; set; }
        public GenderEnum Gender { get; set; } = GenderEnum.Male;
        [Column(TypeName = "date")]
        public DateTime DateOfBirth { get; set; }

        [ForeignKey("National")]
        public int NationalId { get; set; }
        public Country National { get; set; }

        public List<SubjectRole> SubjectRoles { get; set; } = new();

    }
}
