﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Enums;
using Domain.Common.Configurations;

namespace Domain.Entities
{
    public class SubjectRole : Entity
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [Required]
        [ForeignKey("Individual")]
        public string IndividualId { get; set; }
        public Individual Individual { get; set; }

        [Required]
        [ForeignKey("Contract")]
        public string ContractId { get; set; }
        public Contract Contract { get; set; }

        public RoleOfCustomerEnum RoleOfCustomer { get; set; } = RoleOfCustomerEnum.Role1;
        [ForeignKey("GuaranteeAmount")]
        public int GuaranteeAmountId { get; set; }
        public Amount GuaranteeAmount { get; set; }
    }
}
