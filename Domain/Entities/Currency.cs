﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Common.Configurations;

namespace Domain.Entities
{
    public class Currency : Entity
    {
        public Currency()
        {
        }
        public Currency(string code)
        {
            Code = code;
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(3)]
        public string Code { get; set; }

    }
}
