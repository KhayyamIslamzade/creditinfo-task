﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Core.Enums;
using Data.Configurations;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Data
{
    public class ApplicationDbContext : DbContext

    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Amount> Amounts { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Individual> Individuals { get; set; }
        public DbSet<SubjectRole> SubjectRoles { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IHttpContextAccessor httpContextAccessor)
            : base(options)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new CurrencyConfiguration());
            modelBuilder.SetStatusQueryFilter();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaving();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken = default)
        {
            OnBeforeSaving();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void OnBeforeSaving()
        {

            foreach (var entry in ChangeTracker.Entries())
            {
                var propertyNames = entry.Properties.Select(c => c.Metadata.Name).ToList();

                switch (entry.State)
                {
                    case EntityState.Added:
                        if (propertyNames.Any(c => c == "DateCreated"))
                            entry.CurrentValues["DateCreated"] = DateTime.Now;

                        break;
                    case EntityState.Modified:
                        if (propertyNames.Any(c => c == "DateModified"))
                            entry.CurrentValues["DateModified"] = DateTime.Now;
                        break;

                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        if (propertyNames.Any(c => c == "DateDeleted"))
                            entry.CurrentValues["DateDeleted"] = DateTime.Now;
                        if (propertyNames.Any(c => c == "Status"))
                            entry.CurrentValues["Status"] = RecordStatusEnum.Deleted;
                        break;
                }
            }

        }
    }

}
