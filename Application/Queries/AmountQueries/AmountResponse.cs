﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Queries.CurrencyQueries;

namespace Application.Queries.AmountQueries
{
    public class AmountResponse
    {
        public int Id { get; set; }
        public double Value { get; set; }
        public CurrencyResponse Currency { get; set; }
    }
}
