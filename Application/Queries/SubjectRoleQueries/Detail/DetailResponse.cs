﻿using System.Collections.Generic;
using Application.Queries.IndividualQueries;
using Core.Models;

namespace Application.Queries.SubjectRoleQueries.Detail
{
    public class DetailResponse
    {
        public List<IndividualResponse> Response { get; set; }
    }
}
