﻿using System.Threading;
using System.Threading.Tasks;
using Application.Queries.IndividualQueries.GetAllIndividual;
using AutoMapper;
using DataAccess.Repository.SubjectRoleRepository;
using Infrastructure.Configurations.Queries;
using MediatR;

namespace Application.Queries.SubjectRoleQueries.Detail
{
    public class DetailQueryHandler : IQueryHandler<DetailQuery, DetailResponse>
    {
        private readonly IMediator _mediator;

        public DetailQueryHandler(IMediator mediator)
        {
            _mediator = mediator;
        }
        public async Task<DetailResponse> Handle(DetailQuery query, CancellationToken cancellationToken)
        {
            var result = (await _mediator.Send(new GetAllIndividualQuery(new()
            {
                FilterParameters = new()
                {
                    NationalIds = new() { query.Request.NationalId },
                    ContractedOnly = true
                },
                PagingParameters = new()
                {
                    IsAll = true
                }
            }), cancellationToken)).Response.Items;
            return new DetailResponse()
            { Response = result };
        }
    }
}
