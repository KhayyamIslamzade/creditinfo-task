﻿using Infrastructure.Configurations.Queries;

namespace Application.Queries.SubjectRoleQueries.Detail
{
    public class DetailQuery : IQuery<DetailResponse>
    {
        public DetailQuery(DetailRequest request)
        {
            Request = request;
        }

        public DetailRequest Request { get; set; }
    }
}
