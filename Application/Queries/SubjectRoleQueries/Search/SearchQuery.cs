﻿using Infrastructure.Configurations.Queries;

namespace Application.Queries.SubjectRoleQueries.Search
{
    public class SearchQuery : IQuery<SearchResponse>
    {
        public SearchQuery(SearchRequest request)
        {
            Request = request;
        }

        public SearchRequest Request { get; set; }
    }
}
