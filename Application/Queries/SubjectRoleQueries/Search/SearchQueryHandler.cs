﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DataAccess.Repository.SubjectRoleRepository;
using Infrastructure.Configurations.Queries;
using MediatR;

namespace Application.Queries.SubjectRoleQueries.Search
{
    public class SearchQueryHandler : IQueryHandler<SearchQuery, SearchResponse>
    {
        private readonly ISubjectRoleRepository _repository;

        public SearchQueryHandler(ISubjectRoleRepository repository)
        {
            _repository = repository;
        }
        public async Task<SearchResponse> Handle(SearchQuery query, CancellationToken cancellationToken)
        {
            var isExist = await _repository.IsExistAsync(c => c.Individual.NationalId == query.Request.NationalId);
            return new SearchResponse()
            { IsExist = isExist };
        }
    }
}
