﻿namespace Application.Queries.SubjectRoleQueries.Search
{
    public class SearchResponse
    {
        public bool IsExist { get; set; }
        public string Message => IsExist ? "single hit" : "no hit";
    }
}
