﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Queries.AmountQueries;
using Core.Enums;

namespace Application.Queries.SubjectRoleQueries
{
    public class SubjectRoleInfoResponse
    {
        public int Id { get; set; }
        public RoleOfCustomerEnum RoleOfCustomer { get; set; }
        public AmountResponse GuaranteeAmount { get; set; }
    }
}
