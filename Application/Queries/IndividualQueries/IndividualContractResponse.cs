﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Application.Queries.AmountQueries;
using Application.Queries.ContractQueries;
using Application.Queries.SubjectRoleQueries;
using Domain.Common.Enums;
using Domain.Entities;

namespace Application.Queries.IndividualQueries
{
    public class IndividualContractResponse
    {
        public SubjectRoleInfoResponse SubjectRole { get; set; }
        public string ContractCode { get; set; }
        public DateTime DateOfLastPayment { get; set; }
        public DateTime NextPaymentDate { get; set; }
        public DateTime DateAccountOpened { get; set; }
        public string PhaseOfContract { get; set; }
        public AmountResponse OriginalAmount { get; set; }
        public AmountResponse InstallmentAmount { get; set; }

    }
}
