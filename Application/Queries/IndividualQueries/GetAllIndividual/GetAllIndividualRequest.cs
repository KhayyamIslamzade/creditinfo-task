﻿using Application.Queries.ContractQueries;
using Core.Models;

namespace Application.Queries.IndividualQueries.GetAllIndividual
{
    public class GetAllIndividualRequest
    {
        public IndividualFilterParameters FilterParameters { get; set; }
        public PagingParameters PagingParameters { get; set; }
    }
}
