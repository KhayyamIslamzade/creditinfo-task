﻿using Core.Models;

namespace Application.Queries.IndividualQueries.GetAllIndividual
{
    public class GetAllIndividualResponse
    {
        public FilteredDataResult<IndividualResponse> Response { get; set; }
    }
}
