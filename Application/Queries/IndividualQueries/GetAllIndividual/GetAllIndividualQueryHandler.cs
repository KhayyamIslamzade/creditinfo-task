﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Core.Extensions;
using Core.Models;
using DataAccess;
using DataAccess.Repository.ContractRepository;
using DataAccess.Repository.IndividualRepository;
using Domain.Entities;
using Infrastructure.Configurations.Queries;
using Microsoft.EntityFrameworkCore;

namespace Application.Queries.IndividualQueries.GetAllIndividual
{
    public class GetAllIndividualQueryHandler : IQueryHandler<GetAllIndividualQuery, GetAllIndividualResponse>
    {
        private readonly IIndividualRepository _repository;
        private readonly IMapper _mapper;

        public GetAllIndividualQueryHandler(IIndividualRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<GetAllIndividualResponse> Handle(GetAllIndividualQuery query, CancellationToken cancellationToken)
        {

            var filterParameters = query.Request.FilterParameters;

            Expression<Func<Individual, bool>> filterPredicate = c => true;
            //filter by props
            if (filterParameters != null)
            {
                var isNationalIdsExist = filterParameters.NationalIds != null && filterParameters.NationalIds.Any();

                filterPredicate = c =>
                    (!isNationalIdsExist || filterParameters.NationalIds.Contains(c.NationalId)) &&
                    (!filterParameters.ContractedOnly || c.SubjectRoles.Any());
            }

            //get total data count before paging
            var dataCount = await _repository.FindBy(filterPredicate).CountAsync(cancellationToken: cancellationToken);

            var data = _repository.FindBy(filterPredicate, "SubjectRoles.GuaranteeAmount.Currency",
                "SubjectRoles.Contract.OriginalAmount.Currency",
                "SubjectRoles.Contract.InstallmentAmount.Currency");

            //paging
            data = data.FindPaged(query.Request.PagingParameters);

            //mapping
            var result = _mapper.Map<List<Individual>, List<IndividualResponse>>(await data.ToListAsync(cancellationToken: cancellationToken).ConfigureAwait(false));
            return new GetAllIndividualResponse()
            {
                Response = new FilteredDataResult<IndividualResponse>()
                {
                    Items = result,
                    TotalCount = dataCount
                }
            };
        }
    }
}
