﻿using Infrastructure.Configurations.Queries;

namespace Application.Queries.IndividualQueries.GetAllIndividual
{
    public class GetAllIndividualQuery : IQuery<GetAllIndividualResponse>
    {
        public GetAllIndividualQuery(GetAllIndividualRequest request)
        {
            Request = request;
        }

        public GetAllIndividualRequest Request { get; set; }
    }
}
