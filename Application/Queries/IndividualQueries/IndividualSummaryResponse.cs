﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Application.Queries.AmountQueries;
using Application.Queries.ContractQueries;
using Domain.Common.Enums;
using Domain.Entities;

namespace Application.Queries.IndividualQueries
{
    public class IndividualSummaryResponse
    {
        public double SumOriginalAmount { get; set; }
        public double SumInstallmentAmount { get; set; }

    }
}
