﻿using System.Collections.Generic;

namespace Application.Queries.IndividualQueries
{
    public class IndividualFilterParameters
    {
        public List<int> NationalIds { get; set; }
        public bool ContractedOnly { get; set; }
    }
}
