﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Application.Queries.AmountQueries;
using Application.Queries.ContractQueries;
using Domain.Common.Enums;
using Domain.Entities;

namespace Application.Queries.IndividualQueries
{
    public class IndividualResponse
    {
        public string CustomerCode { get; set; }
        public string Surname { get; set; }
        public string Lastname { get; set; }
        public GenderEnum Gender { get; set; } = GenderEnum.Male;
        public DateTime DateOfBirth { get; set; }

        public int NationalId { get; set; }
        public List<IndividualContractResponse> Contracts { get; set; } = new();

    }
}
