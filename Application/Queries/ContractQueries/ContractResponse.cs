﻿using System;
using Application.Queries.AmountQueries;
using Domain.Entities;

namespace Application.Queries.ContractQueries
{
    public class ContractResponse
    {
        public string ContractCode { get; set; }
        public DateTime DateOfLastPayment { get; set; }
        public DateTime NextPaymentDate { get; set; }
        public DateTime DateAccountOpened { get; set; }
        public string PhaseOfContract { get; set; }
        public AmountResponse OriginalAmount { get; set; }
        public AmountResponse InstallmentAmount { get; set; }
    }
}
