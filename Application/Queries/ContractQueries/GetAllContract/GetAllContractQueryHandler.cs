﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Application.Queries.IndividualQueries;
using AutoMapper;
using Core.Extensions;
using Core.Models;
using DataAccess;
using DataAccess.Repository.ContractRepository;
using Domain.Entities;
using Infrastructure.Configurations.Queries;
using Microsoft.EntityFrameworkCore;

namespace Application.Queries.ContractQueries.GetAllContract
{
    public class GetAllContractQueryHandler : IQueryHandler<GetAllContractQuery, GetAllContractResponse>
    {
        private readonly IContractRepository _repository;
        private readonly IMapper _mapper;

        public GetAllContractQueryHandler(IContractRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<GetAllContractResponse> Handle(GetAllContractQuery query, CancellationToken cancellationToken)
        {

            var filterParameters = query.Request.FilterParameters;

            Expression<Func<Contract, bool>> filterPredicate = c => true;
            //filter by props
            if (filterParameters != null)
            {
                var isIdsExist = filterParameters.ContractCodes != null && filterParameters.ContractCodes.Any();

                filterPredicate = c =>
                    (!isIdsExist || filterParameters.ContractCodes.Contains(c.ContractCode));
            }

            //get total data count before paging
            var dataCount = await _repository.FindBy(filterPredicate).CountAsync(cancellationToken: cancellationToken);

            var data = _repository.FindBy(filterPredicate, "OriginalAmount.Currency", "InstallmentAmount.Currency");

            //sorting
            data = data.SortBy(query.Request.SortParameters);

            //paging
            data = data.FindPaged(query.Request.PagingParameters);

            //mapping
            var result = _mapper.Map<List<Contract>, List<IndividualResponse>>(await data.ToListAsync(cancellationToken: cancellationToken).ConfigureAwait(false));
            return new GetAllContractResponse()
            {
                Response = new FilteredDataResult<IndividualResponse>()
                {
                    Items = result,
                    TotalCount = dataCount
                }
            };
        }
    }
}
