﻿using Application.Queries.IndividualQueries;
using Core.Models;

namespace Application.Queries.ContractQueries.GetAllContract
{
    public class GetAllContractResponse
    {
        public FilteredDataResult<IndividualResponse> Response { get; set; }
    }
}
