﻿using Application.Queries.IndividualQueries;
using Core.Models;

namespace Application.Queries.ContractQueries.GetAllContract
{
    public class GetAllContractRequest
    {
        public ContractFilterParameters FilterParameters { get; set; }
        public PagingParameters PagingParameters { get; set; }
        public SortParameters SortParameters { get; set; }
    }
}
