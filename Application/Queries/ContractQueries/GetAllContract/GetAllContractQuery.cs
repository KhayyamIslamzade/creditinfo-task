﻿using Infrastructure.Configurations.Queries;

namespace Application.Queries.ContractQueries.GetAllContract
{
    public class GetAllContractQuery : IQuery<GetAllContractResponse>
    {
        public GetAllContractQuery(GetAllContractRequest request)
        {
            Request = request;
        }

        public GetAllContractRequest Request { get; set; }
    }
}
