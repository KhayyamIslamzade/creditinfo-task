﻿using System.Collections.Generic;

namespace Application.Queries.ContractQueries
{
    public class ContractFilterParameters
    {
        public List<string> ContractCodes { get; set; }
    }
}
