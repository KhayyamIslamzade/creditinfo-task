﻿using Domain.Entities;

namespace Application.Commands.SubjectRoleCommands.CreateSubjectRole
{
    public class CreateSubjectRoleResponse
    {
        public SubjectRole Response { get; set; }
    }
}
