﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Core.Exceptions;
using DataAccess.Repository;
using DataAccess.Repository.ContractRepository;
using DataAccess.Repository.CountryRepository;
using DataAccess.Repository.CurrencyRepository;
using DataAccess.Repository.IndividualRepository;
using DataAccess.Repository.SubjectRoleRepository;
using Domain.Entities;
using Infrastructure.Configurations.Commands;
using MediatR;

namespace Application.Commands.SubjectRoleCommands.CreateSubjectRole
{
    public class CreateSubjectRoleCommandHandler : ICommandHandler<CreateSubjectRoleCommand, CreateSubjectRoleResponse>
    {
        private readonly ISubjectRoleRepository _repository;
        private readonly IContractRepository _contractRepository;
        private readonly IIndividualRepository _individualRepository;
        private readonly ICurrencyRepository _currencyRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMediator _mediator;

        public CreateSubjectRoleCommandHandler(ISubjectRoleRepository repository, IContractRepository contractRepository, IIndividualRepository individualRepository, ICurrencyRepository currencyRepository, IMapper mapper, IUnitOfWork unitOfWork, IMediator mediator)
        {
            _repository = repository;
            _contractRepository = contractRepository;
            _individualRepository = individualRepository;
            _currencyRepository = currencyRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _mediator = mediator;
        }
        public async Task<CreateSubjectRoleResponse> Handle(CreateSubjectRoleCommand command, CancellationToken cancellationToken)
        {

            var isExist = await _repository.IsExistAsync(c => c.ContractId == command.Request.ContractId && c.IndividualId == command.Request.IndividualId && c.RoleOfCustomer == command.Request.RoleOfCustomer);
            if (isExist)
                throw new RecordAlreadyExistException();

            var contract =
                await _contractRepository.GetFirstAsync(c => c.ContractCode == command.Request.ContractId);
            if (contract == null)
                throw new RecordAlreadyExistException();

            var individual =
                await _individualRepository.GetFirstAsync(c => c.CustomerCode == command.Request.IndividualId);
            if (individual == null)
                throw new RecordAlreadyExistException();

            var currency =
                await _currencyRepository.GetFirstAsync(c => c.Id == command.Request.GuaranteeAmountCurrencyId);
            if (currency == null)
                throw new RecordAlreadyExistException();


            var data = _mapper.Map<CreateSubjectRoleRequest, SubjectRole>(command.Request);
            data.GuaranteeAmount = new Amount()
            {
                Value = command.Request.GuaranteeAmountValue,
                CurrencyId = command.Request.GuaranteeAmountCurrencyId
            };
            await _repository.AddAsync(data);
            await _unitOfWork.CompleteAsync(cancellationToken).ConfigureAwait(false);
            return new CreateSubjectRoleResponse()
            {
                Response = data
            };
        }
    }
}
