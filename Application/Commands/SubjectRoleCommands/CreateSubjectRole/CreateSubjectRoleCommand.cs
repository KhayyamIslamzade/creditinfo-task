﻿using Infrastructure.Configurations.Commands;

namespace Application.Commands.SubjectRoleCommands.CreateSubjectRole
{
    public class CreateSubjectRoleCommand : CommandBase<CreateSubjectRoleResponse>
    {
        public CreateSubjectRoleCommand(CreateSubjectRoleRequest request)
        {
            Request = request;
        }

        public CreateSubjectRoleRequest Request { get; set; }
    }
}
