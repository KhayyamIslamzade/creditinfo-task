﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Enums;
using Domain.Common.Enums;
using Domain.Entities;

namespace Application.Commands.SubjectRoleCommands.CreateSubjectRole
{
    public class CreateSubjectRoleRequest
    {
        public string IndividualId { get; set; }
        public string ContractId { get; set; }

        public RoleOfCustomerEnum RoleOfCustomer { get; set; } = RoleOfCustomerEnum.Role1;
        public double GuaranteeAmountValue { get; set; }
        public int GuaranteeAmountCurrencyId { get; set; }
    }
}
