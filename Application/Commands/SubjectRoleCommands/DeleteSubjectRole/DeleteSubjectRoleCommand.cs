﻿using Infrastructure.Configurations.Commands;

namespace Application.Commands.SubjectRoleCommands.DeleteSubjectRole
{
    public class DeleteSubjectRoleCommand : CommandBase<DeleteSubjectRoleResponse>
    {
        public DeleteSubjectRoleCommand(DeleteSubjectRoleRequest request)
        {
            Request = request;
        }

        public DeleteSubjectRoleRequest Request { get; set; }
    }
}
