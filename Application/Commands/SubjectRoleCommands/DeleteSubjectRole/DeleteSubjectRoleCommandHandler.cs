﻿using System.Threading;
using System.Threading.Tasks;
using Core.Exceptions;
using DataAccess.Repository;
using DataAccess.Repository.SubjectRoleRepository;
using Infrastructure.Configurations.Commands;

namespace Application.Commands.SubjectRoleCommands.DeleteSubjectRole
{
    public class DeleteSubjectRoleCommandHandler : ICommandHandler<DeleteSubjectRoleCommand, DeleteSubjectRoleResponse>
    {
        private readonly ISubjectRoleRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteSubjectRoleCommandHandler(ISubjectRoleRepository repository, IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }
        public async Task<DeleteSubjectRoleResponse> Handle(DeleteSubjectRoleCommand command, CancellationToken cancellationToken)
        {
            var data = await _repository.GetFirstAsync(c => c.Id == command.Request.Id).ConfigureAwait(false);
            if (data == null)
                throw new RecordNotFoundException();

            _repository.Delete(data);
            await _unitOfWork.CompleteAsync(cancellationToken).ConfigureAwait(false);
            return new DeleteSubjectRoleResponse();
        }
    }
}
