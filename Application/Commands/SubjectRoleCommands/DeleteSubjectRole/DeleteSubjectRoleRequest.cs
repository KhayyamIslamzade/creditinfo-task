﻿namespace Application.Commands.SubjectRoleCommands.DeleteSubjectRole
{
    public class DeleteSubjectRoleRequest
    {
        public DeleteSubjectRoleRequest(int id)
        {
            Id = id;
        }

        public int Id { get; set; }
    }
}
