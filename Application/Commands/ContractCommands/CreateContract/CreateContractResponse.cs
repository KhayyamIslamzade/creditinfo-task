﻿using Domain.Entities;

namespace Application.Commands.ContractCommands.CreateContract
{
    public class CreateContractResponse
    {
        public Contract Response { get; set; }
    }
}
