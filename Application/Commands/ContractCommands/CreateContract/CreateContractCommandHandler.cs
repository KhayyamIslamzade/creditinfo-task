﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Core.Exceptions;
using DataAccess.Repository;
using DataAccess.Repository.ContractRepository;
using DataAccess.Repository.CurrencyRepository;
using Domain.Entities;
using Infrastructure.Configurations.Commands;
using MediatR;

namespace Application.Commands.ContractCommands.CreateContract
{
    public class CreateContractCommandHandler : ICommandHandler<CreateContractCommand, CreateContractResponse>
    {
        private readonly IContractRepository _repository;
        private readonly ICurrencyRepository _currencyRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMediator _mediator;

        public CreateContractCommandHandler(IContractRepository repository, ICurrencyRepository currencyRepository, IMapper mapper, IUnitOfWork unitOfWork, IMediator mediator)
        {
            _repository = repository;
            _currencyRepository = currencyRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _mediator = mediator;
        }
        public async Task<CreateContractResponse> Handle(CreateContractCommand command, CancellationToken cancellationToken)
        {



            var isExist = await _repository.IsExistAsync(c => c.ContractCode == command.Request.ContractCode);
            if (isExist)
                throw new RecordAlreadyExistException();

            var originalCurrency =
                await _currencyRepository.GetFirstAsync(c => c.Id == command.Request.OriginalCurrencyId);
            if (originalCurrency == null)
                throw new RecordNotFoundException();

            var installmentCurrency =
                await _currencyRepository.GetFirstAsync(c => c.Id == command.Request.InstallmentCurrencyId);
            if (installmentCurrency == null)
                throw new RecordNotFoundException();

            var data = _mapper.Map<CreateContractRequest, Contract>(command.Request);
            data.InstallmentAmount = new Amount()
            {
                Value = command.Request.InstallmentValue,
                CurrencyId = command.Request.InstallmentCurrencyId
            };
            data.OriginalAmount = new Amount()
            {
                Value = command.Request.OriginalAmountValue,
                CurrencyId = command.Request.OriginalCurrencyId

            };
            //var data = new Contract()
            //{
            //    ContractCode = command.Request.ContractCode,
            //    DateAccountOpened = command.Request.DateAccountOpened,
            //    DateOfLastPayment = command.Request.DateOfLastPayment,
            //    NextPaymentDate = command.Request.NextPaymentDate,
            //    InstallmentAmount = new Amount()
            //    {
            //        Value = command.Request.InstallmentValue,
            //        CurrencyId = command.Request.InstallmentCurrencyId
            //    },
            //    OriginalAmount = new Amount()
            //    {
            //        Value = command.Request.OriginalAmountValue,
            //        CurrencyId = command.Request.OriginalCurrencyId
            //    },
            //    PhaseOfContract = command.Request.PhaseOfContract,
            //};

            await _repository.AddAsync(data);
            await _unitOfWork.CompleteAsync(cancellationToken).ConfigureAwait(false);
            return new CreateContractResponse()
            {
                Response = data
            };
        }
    }
}
