﻿using Infrastructure.Configurations.Commands;

namespace Application.Commands.ContractCommands.CreateContract
{
    public class CreateContractCommand : CommandBase<CreateContractResponse>
    {
        public CreateContractCommand(CreateContractRequest request)
        {
            Request = request;
        }

        public CreateContractRequest Request { get; set; }
    }
}
