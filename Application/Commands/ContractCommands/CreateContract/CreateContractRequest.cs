﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Application.Commands.ContractCommands.CreateContract
{
    public class CreateContractRequest
    {
        [Required]
        public string ContractCode { get; set; }
        public DateTime DateOfLastPayment { get; set; }
        public DateTime NextPaymentDate { get; set; }
        public DateTime DateAccountOpened { get; set; }

        [Required]
        [StringLength(1024)]
        public string PhaseOfContract { get; set; }

        [Range(0, Double.MaxValue)]
        public double OriginalAmountValue { get; set; }
        [Range(0, Double.MaxValue)]
        public double InstallmentValue { get; set; }
        [Range(0, Int32.MaxValue)]
        public int OriginalCurrencyId { get; set; }
        [Range(0, Int32.MaxValue)]
        public int InstallmentCurrencyId { get; set; }

    }
}
