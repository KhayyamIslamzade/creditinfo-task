﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Core.Exceptions;
using DataAccess.Repository;
using DataAccess.Repository.ContractRepository;
using DataAccess.Repository.CurrencyRepository;
using Infrastructure.Configurations.Commands;
using MediatR;

namespace Application.Commands.ContractCommands.UpdateContract
{
    public class UpdateContractCommandHandler : ICommandHandler<UpdateContractCommand, UpdateContractResponse>
    {
        private readonly IContractRepository _repository;
        private readonly ICurrencyRepository _currencyRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateContractCommandHandler(IContractRepository repository, ICurrencyRepository currencyRepository, IMapper mapper, IUnitOfWork unitOfWork, IMediator mediator)
        {
            _repository = repository;
            _currencyRepository = currencyRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<UpdateContractResponse> Handle(UpdateContractCommand command, CancellationToken cancellationToken)
        {

            var originalCurrency =
                await _currencyRepository.GetFirstAsync(c => c.Id == command.Request.OriginalCurrencyId);
            if (originalCurrency == null)
                throw new RecordNotFoundException();

            var installmentCurrency =
                await _currencyRepository.GetFirstAsync(c => c.Id == command.Request.InstallmentCurrencyId);
            if (installmentCurrency == null)
                throw new RecordNotFoundException();



            var data = await _repository.GetFirstAsync(c => c.ContractCode == command.Request.ContractCode, "InstallmentAmount", "OriginalAmount").ConfigureAwait(false);
            if (data == null)
                throw new RecordNotFoundException();

            //update
            _mapper.Map(command.Request, data);
            data.InstallmentAmount.Value = command.Request.InstallmentValue;
            data.InstallmentAmount.CurrencyId = command.Request.InstallmentCurrencyId;

            data.OriginalAmount.Value = command.Request.OriginalAmountValue;
            data.OriginalAmount.CurrencyId = command.Request.OriginalCurrencyId;

            await _unitOfWork.CompleteAsync(cancellationToken).ConfigureAwait(false);
            return new UpdateContractResponse()
            {
                Response = data
            };
        }
    }
}
