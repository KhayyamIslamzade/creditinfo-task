﻿using Domain.Entities;

namespace Application.Commands.ContractCommands.UpdateContract
{
    public class UpdateContractResponse
    {
        public Contract Response { get; set; }
    }
}
