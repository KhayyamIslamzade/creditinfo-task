﻿using Infrastructure.Configurations.Commands;

namespace Application.Commands.ContractCommands.UpdateContract
{
    public class UpdateContractCommand : CommandBase<UpdateContractResponse>
    {
        public UpdateContractCommand(UpdateContractRequest request)
        {
            Request = request;
        }

        public UpdateContractRequest Request { get; set; }
    }
}
