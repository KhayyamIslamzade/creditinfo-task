﻿namespace Application.Commands.ContractCommands.DeleteContract
{
    public class DeleteContractRequest
    {
        public DeleteContractRequest(string contractCode)
        {
            ContractCode = contractCode;
        }

        public string ContractCode { get; set; }
    }
}
