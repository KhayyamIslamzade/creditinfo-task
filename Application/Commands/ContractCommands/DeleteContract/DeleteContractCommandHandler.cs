﻿using System.Threading;
using System.Threading.Tasks;
using Core.Exceptions;
using DataAccess.Repository;
using DataAccess.Repository.ContractRepository;
using Infrastructure.Configurations.Commands;

namespace Application.Commands.ContractCommands.DeleteContract
{
    public class DeleteContractCommandHandler : ICommandHandler<DeleteContractCommand, DeleteContractResponse>
    {
        private readonly IContractRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteContractCommandHandler(IContractRepository repository, IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }
        public async Task<DeleteContractResponse> Handle(DeleteContractCommand command, CancellationToken cancellationToken)
        {
            var data = await _repository.GetFirstAsync(c => c.ContractCode == command.Request.ContractCode).ConfigureAwait(false);
            if (data == null)
                throw new RecordNotFoundException();

            _repository.Delete(data);
            await _unitOfWork.CompleteAsync(cancellationToken).ConfigureAwait(false);
            return new DeleteContractResponse();
        }
    }
}
