﻿using Infrastructure.Configurations.Commands;

namespace Application.Commands.ContractCommands.DeleteContract
{
    public class DeleteContractCommand : CommandBase<DeleteContractResponse>
    {
        public DeleteContractCommand(DeleteContractRequest request)
        {
            Request = request;
        }

        public DeleteContractRequest Request { get; set; }
    }
}
