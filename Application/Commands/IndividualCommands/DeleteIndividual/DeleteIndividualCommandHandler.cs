﻿using System.Threading;
using System.Threading.Tasks;
using Core.Exceptions;
using DataAccess.Repository;
using DataAccess.Repository.ContractRepository;
using DataAccess.Repository.IndividualRepository;
using Infrastructure.Configurations.Commands;

namespace Application.Commands.IndividualCommands.DeleteIndividual
{
    public class DeleteIndividualCommandHandler : ICommandHandler<DeleteIndividualCommand, DeleteIndividualResponse>
    {
        private readonly IIndividualRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteIndividualCommandHandler(IIndividualRepository repository, IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }
        public async Task<DeleteIndividualResponse> Handle(DeleteIndividualCommand command, CancellationToken cancellationToken)
        {
            var data = await _repository.GetFirstAsync(c => c.CustomerCode == command.Request.CustomerCode).ConfigureAwait(false);
            if (data == null)
                throw new RecordNotFoundException();

            _repository.Delete(data);
            await _unitOfWork.CompleteAsync(cancellationToken).ConfigureAwait(false);
            return new DeleteIndividualResponse();
        }
    }
}
