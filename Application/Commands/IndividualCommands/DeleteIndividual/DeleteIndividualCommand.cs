﻿using Infrastructure.Configurations.Commands;

namespace Application.Commands.IndividualCommands.DeleteIndividual
{
    public class DeleteIndividualCommand : CommandBase<DeleteIndividualResponse>
    {
        public DeleteIndividualCommand(DeleteIndividualRequest request)
        {
            Request = request;
        }

        public DeleteIndividualRequest Request { get; set; }
    }
}
