﻿namespace Application.Commands.IndividualCommands.DeleteIndividual
{
    public class DeleteIndividualRequest
    {
        public DeleteIndividualRequest(string customerCode)
        {
            CustomerCode = customerCode;
        }

        public string CustomerCode { get; set; }
    }
}
