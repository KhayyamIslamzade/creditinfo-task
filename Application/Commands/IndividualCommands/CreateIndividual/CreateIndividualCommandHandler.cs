﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Core.Exceptions;
using DataAccess.Repository;
using DataAccess.Repository.ContractRepository;
using DataAccess.Repository.CountryRepository;
using DataAccess.Repository.CurrencyRepository;
using DataAccess.Repository.IndividualRepository;
using Domain.Entities;
using Infrastructure.Configurations.Commands;
using MediatR;

namespace Application.Commands.IndividualCommands.CreateIndividual
{
    public class CreateIndividualCommandHandler : ICommandHandler<CreateIndividualCommand, CreateIndividualResponse>
    {
        private readonly IIndividualRepository _repository;
        private readonly ICountryRepository _countryRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMediator _mediator;

        public CreateIndividualCommandHandler(IIndividualRepository repository, ICountryRepository countryRepository, IMapper mapper, IUnitOfWork unitOfWork, IMediator mediator)
        {
            _repository = repository;
            _countryRepository = countryRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _mediator = mediator;
        }
        public async Task<CreateIndividualResponse> Handle(CreateIndividualCommand command, CancellationToken cancellationToken)
        {



            var isExist = await _repository.IsExistAsync(c => c.CustomerCode == command.Request.CustomerCode);
            if (isExist)
                throw new RecordAlreadyExistException();

            var national =
                await _countryRepository.GetFirstAsync(c => c.Id == command.Request.NationalId);
            if (national == null)
                throw new RecordNotFoundException();

            var data = _mapper.Map<CreateIndividualRequest, Individual>(command.Request);

            await _repository.AddAsync(data);
            await _unitOfWork.CompleteAsync(cancellationToken).ConfigureAwait(false);
            return new CreateIndividualResponse()
            {
                Response = data
            };
        }
    }
}
