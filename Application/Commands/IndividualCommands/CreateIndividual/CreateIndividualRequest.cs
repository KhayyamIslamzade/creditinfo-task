﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Common.Enums;
using Domain.Entities;

namespace Application.Commands.IndividualCommands.CreateIndividual
{
    public class CreateIndividualRequest
    {
        [Required]
        public string CustomerCode { get; set; }
        [Required]
        [StringLength(128)]
        public string Surname { get; set; }
        [Required]
        [StringLength(128)]
        public string Lastname { get; set; }
        public GenderEnum Gender { get; set; } = GenderEnum.Male;
        public DateTime DateOfBirth { get; set; }
        public int NationalId { get; set; }
    }
}
