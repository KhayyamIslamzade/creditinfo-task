﻿using Domain.Entities;

namespace Application.Commands.IndividualCommands.CreateIndividual
{
    public class CreateIndividualResponse
    {
        public Individual Response { get; set; }
    }
}
