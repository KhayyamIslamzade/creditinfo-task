﻿using Infrastructure.Configurations.Commands;

namespace Application.Commands.IndividualCommands.CreateIndividual
{
    public class CreateIndividualCommand : CommandBase<CreateIndividualResponse>
    {
        public CreateIndividualCommand(CreateIndividualRequest request)
        {
            Request = request;
        }

        public CreateIndividualRequest Request { get; set; }
    }
}
