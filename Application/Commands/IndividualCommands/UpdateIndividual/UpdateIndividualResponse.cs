﻿using Domain.Entities;

namespace Application.Commands.IndividualCommands.UpdateIndividual
{
    public class UpdateIndividualResponse
    {
        public Individual Response { get; set; }
    }
}
