﻿using System;
using System.ComponentModel.DataAnnotations;
using Domain.Common.Enums;

namespace Application.Commands.IndividualCommands.UpdateIndividual
{
    public class UpdateIndividualRequest
    {
        [Required]
        public string CustomerCode { get; set; }
        [Required]
        [StringLength(128)]
        public string Surname { get; set; }
        [Required]
        [StringLength(128)]
        public string Lastname { get; set; }
        public GenderEnum Gender { get; set; } = GenderEnum.Male;
        public DateTime DateOfBirth { get; set; }
        public int NationalId { get; set; }
    }
}
