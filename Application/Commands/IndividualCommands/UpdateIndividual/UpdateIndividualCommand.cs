﻿using Infrastructure.Configurations.Commands;

namespace Application.Commands.IndividualCommands.UpdateIndividual
{
    public class UpdateIndividualCommand : CommandBase<UpdateIndividualResponse>
    {
        public UpdateIndividualCommand(UpdateIndividualRequest request)
        {
            Request = request;
        }

        public UpdateIndividualRequest Request { get; set; }
    }
}
