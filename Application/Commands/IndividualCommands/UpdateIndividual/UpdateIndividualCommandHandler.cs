﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Core.Exceptions;
using DataAccess.Repository;
using DataAccess.Repository.ContractRepository;
using DataAccess.Repository.CountryRepository;
using DataAccess.Repository.IndividualRepository;
using Infrastructure.Configurations.Commands;
using MediatR;

namespace Application.Commands.IndividualCommands.UpdateIndividual
{
    public class UpdateIndividualCommandHandler : ICommandHandler<UpdateIndividualCommand, UpdateIndividualResponse>
    {
        private readonly IIndividualRepository _repository;
        private readonly ICountryRepository _countryRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMediator _mediator;
        public UpdateIndividualCommandHandler(IIndividualRepository repository, ICountryRepository countryRepository, IMapper mapper, IUnitOfWork unitOfWork, IMediator mediator)
        {
            _repository = repository;
            _countryRepository = countryRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _mediator = mediator;
        }
        public async Task<UpdateIndividualResponse> Handle(UpdateIndividualCommand command, CancellationToken cancellationToken)
        {
            var data = await _repository.GetFirstAsync(c => c.CustomerCode == command.Request.CustomerCode).ConfigureAwait(false);
            if (data == null)
                throw new RecordNotFoundException();

            var national =
                await _countryRepository.GetFirstAsync(c => c.Id == command.Request.NationalId);
            if (national == null)
                throw new RecordNotFoundException();

            //update
            _mapper.Map(command.Request, data);

            await _unitOfWork.CompleteAsync(cancellationToken).ConfigureAwait(false);
            return new UpdateIndividualResponse()
            {
                Response = data
            };
        }
    }
}
