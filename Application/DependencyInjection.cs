﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using AutoWrapper;
using Data;
using DataAccess.Repository;
using Infrastructure.Pipelines;
using Infrastructure.Services;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;

namespace Application
{

    public class DependencyInjectionOptions
    {
        public Assembly[] AutoMapperAssemblies { get; set; }
        public Assembly SwaggerAssembly { get; set; }
    }

    public static class DependencyInjection
    {
        private static readonly bool IsDevelopment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == Environments.Development;
        public static IServiceCollection ConfigureDependencyInjections(this IServiceCollection services,
            IConfiguration configuration, DependencyInjectionOptions options)
        {
            services
                .ConfigureDatabase(configuration)
                .ConfigureCors()
                .AddHttpClient()
                .AddRepositories()
                .ConfigureAutoMapper(options.AutoMapperAssemblies)
                .ConfigureServices()
                .AddSwagger(options.SwaggerAssembly)
                .AddMediator();
            return services;
        }
        public static IServiceCollection AddSwagger(this IServiceCollection services, Assembly assembly)
        {
            services.AddSwaggerGen(swagger =>
            {
                //This is to generate the Default UI of Swagger Documentation  
                swagger.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "creditinfo-task",
                    Version = "v1",
                    Description = "ASP.NET Core 5 Web API"
                });


                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{assembly.GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                swagger.IncludeXmlComments(xmlPath);

            });
            services.AddSwaggerGenNewtonsoftSupport();

            return services;
        }

        public static IServiceCollection AddMediator(this IServiceCollection services)
        {
            services
                .AddMediatR(Assembly.GetExecutingAssembly())
                .AddTransient(typeof(IPipelineBehavior<,>), typeof(TransactionPipelineBehavior<,>));
            return services;
        }
        public static IApplicationBuilder ConfigureAutoWrapperMiddleware(this IApplicationBuilder builder)
        {
            builder.UseApiResponseAndExceptionWrapper(new AutoWrapperOptions
            {
                //UseApiProblemDetailsException = true,
                IgnoreNullValue = false,
                ShowStatusCode = true,
                ShowIsErrorFlagForSuccessfulResponse = true,
                //IgnoreWrapForOkRequests = true,
                IsDebug = IsDevelopment,

                EnableExceptionLogging = false,
                EnableResponseLogging = false,
                LogRequestDataOnException = false,
                ShouldLogRequestData = false,


                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                UseCustomExceptionFormat = false,
            });

            return builder;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            // i assume your service interfaces inherit from IRepositoryBase<>
            Assembly ass = typeof(IRepositoryIdentifier).GetTypeInfo().Assembly;

            // get all concrete types which implements IRepositoryIdentifier
            var allRepositories = ass.GetTypes().Where(t =>
                t.GetTypeInfo().IsClass &&
                !t.IsGenericType &&
                !t.GetTypeInfo().IsAbstract &&
                typeof(IRepositoryIdentifier).IsAssignableFrom(t));

            foreach (var type in allRepositories)
            {
                var allInterfaces = type.GetInterfaces();
                var mainInterfaces = allInterfaces.Where(t => typeof(IRepositoryIdentifier) != t && (!t.IsGenericType || t.GetGenericTypeDefinition() != typeof(IRepository<>)));
                foreach (var itype in mainInterfaces)
                {
                    if (allRepositories.Any(x => x != type && itype.IsAssignableFrom(x)))
                    {
                        throw new Exception("The " + itype.Name + " type has more than one implementations, please change your filter");
                    }
                    services.AddScoped(itype, type);
                }
            }
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            return services;
        }
        private static IServiceCollection ConfigureAutoMapper(this IServiceCollection services, Assembly[] assemblies)
        {
            services.AddAutoMapper(assemblies);
            return services;
        }
        public static IServiceCollection ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {

                options.AddDefaultPolicy(builder =>
                {
                    builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                });

            });
            return services;
        }

        private static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            services.AddHttpContextAccessor();
            services.AddScoped<FileService>();
            services.AddScoped<XmlValidationService>();
            return services;
        }

        private static IServiceCollection ConfigureDatabase(this IServiceCollection services,
            IConfiguration configuration)
        {

            services.AddDbContext<ApplicationDbContext>(
                options => options.UseNpgsql(configuration.GetConnectionString("ConnectionString")));

            //services.AddDbContext<ApplicationDbContext>(
            //    options => options.UseSqlServer(configuration.GetConnectionString("ConnectionString")));

            return services;
        }

    }
}
