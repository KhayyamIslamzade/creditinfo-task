﻿using Application.Queries.AmountQueries;
using Application.Queries.CurrencyQueries;
using AutoMapper;
using Domain.Entities;

namespace Application.Mapping
{
    public class CurrencyMappingProfile : Profile
    {
        public CurrencyMappingProfile()
        {
            CreateMap<Currency, CurrencyResponse>();

        }
    }
}
