﻿using Application.Commands.ContractCommands.CreateContract;
using Application.Commands.ContractCommands.UpdateContract;
using AutoMapper;
using Domain.Entities;

namespace Application.Mapping
{
    public class ContractMappingProfile : Profile
    {
        public ContractMappingProfile()
        {
            //CreateMap<Contract, ContractResponse>();
            CreateMap<CreateContractRequest, Contract>()
                .ForMember(c => c.InstallmentAmount, opt => opt.Ignore())
                .ForMember(c => c.OriginalAmount, opt => opt.Ignore());
            CreateMap<UpdateContractRequest, Contract>().ForMember(c => c.InstallmentAmount, opt => opt.Ignore())
                .ForMember(c => c.OriginalAmount, opt => opt.Ignore());

        }
    }
}
