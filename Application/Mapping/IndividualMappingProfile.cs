﻿using Application.Commands.IndividualCommands.CreateIndividual;
using Application.Commands.IndividualCommands.UpdateIndividual;
using Application.Queries.AmountQueries;
using Application.Queries.CurrencyQueries;
using Application.Queries.IndividualQueries;
using Application.Queries.SubjectRoleQueries;
using AutoMapper;
using DataAccess.Repository.SubjectRoleRepository;
using Domain.Entities;

namespace Application.Mapping
{
    public class IndividualMappingProfile : Profile
    {
        public IndividualMappingProfile()
        {
            CreateMap<Individual, IndividualResponse>()
                .ForMember(c => c.Contracts, opt => opt.MapFrom(e => e.SubjectRoles));

            CreateMap<SubjectRole, IndividualContractResponse>()
                .ForMember(c => c.ContractCode, opt => opt.MapFrom(e => e.Contract.ContractCode))
                .ForMember(c => c.DateAccountOpened, opt => opt.MapFrom(e => e.Contract.DateAccountOpened))
                .ForMember(c => c.DateOfLastPayment, opt => opt.MapFrom(e => e.Contract.DateOfLastPayment))
                .ForMember(c => c.NextPaymentDate, opt => opt.MapFrom(e => e.Contract.NextPaymentDate))
                .ForMember(c => c.PhaseOfContract, opt => opt.MapFrom(e => e.Contract.PhaseOfContract))
                .ForMember(c => c.InstallmentAmount, opt => opt.MapFrom(e => new AmountResponse()
                {
                    Id = e.Contract.InstallmentAmount.Id,
                    Value = e.Contract.InstallmentAmount.Value,
                    Currency = new CurrencyResponse()
                    {
                        Id = e.Contract.InstallmentAmount.Currency.Id,
                        Code = e.Contract.InstallmentAmount.Currency.Code,
                    }
                }))
                .ForMember(c => c.OriginalAmount, opt => opt.MapFrom(e => new AmountResponse()
                {
                    Id = e.Contract.OriginalAmount.Id,
                    Value = e.Contract.OriginalAmount.Value,
                    Currency = new CurrencyResponse()
                    {
                        Id = e.Contract.OriginalAmount.Currency.Id,
                        Code = e.Contract.OriginalAmount.Currency.Code,
                    }
                }))

                .ForMember(c => c.SubjectRole, opt => opt.MapFrom(e => new SubjectRoleInfoResponse()
                {
                    Id = e.Id,
                    RoleOfCustomer = e.RoleOfCustomer,
                    GuaranteeAmount = new AmountResponse()
                    {
                        Id = e.GuaranteeAmount.Id,
                        Value = e.GuaranteeAmount.Value,
                        Currency = new CurrencyResponse()
                        {
                            Id = e.GuaranteeAmount.Currency.Id,
                            Code = e.GuaranteeAmount.Currency.Code,
                        }
                    },
                }));



            CreateMap<CreateIndividualRequest, Individual>();
            CreateMap<UpdateIndividualRequest, Individual>();


        }
    }
}
