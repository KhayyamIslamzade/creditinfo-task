﻿using Application.Commands.IndividualCommands.CreateIndividual;
using Application.Commands.IndividualCommands.UpdateIndividual;
using Application.Commands.SubjectRoleCommands.CreateSubjectRole;
using AutoMapper;
using Domain.Entities;

namespace Application.Mapping
{
    public class SubjectRoleMappingProfile : Profile
    {
        public SubjectRoleMappingProfile()
        {
            //CreateMap<Individual, IndividualResponse>();
            CreateMap<CreateSubjectRoleRequest, SubjectRole>();


        }
    }
}
