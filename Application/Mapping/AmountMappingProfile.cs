﻿using Application.Queries.AmountQueries;
using AutoMapper;
using Domain.Entities;

namespace Application.Mapping
{
    public class AmountMappingProfile : Profile
    {
        public AmountMappingProfile()
        {
            CreateMap<Amount, AmountResponse>();

        }
    }
}
