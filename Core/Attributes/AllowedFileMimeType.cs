﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;

namespace Core.Attributes
{

    public class AllowedFileMimeType : ValidationAttribute
    {
        private readonly string[] _contentTypes;

        public AllowedFileMimeType(params string[] contentTypes)
        {
            _contentTypes = contentTypes;
        }

        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            if (value is IFormFile file)
            {
                var contentType = file.ContentType;
                if (!_contentTypes.Contains(contentType))
                {
                    return new ValidationResult(ErrorMessage);
                }
            }
            else if (value is IEnumerable<IFormFile> files)
            {
                foreach (var fileItem in files)
                {
                    var contentType = fileItem.ContentType;
                    if (!_contentTypes.Contains(contentType))
                    {
                        return new ValidationResult(ErrorMessage);
                    }
                }
            }

            return ValidationResult.Success;
        }

    }
}
