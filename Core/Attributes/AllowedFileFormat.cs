﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;

namespace Core.Attributes
{
    public class AllowedFileFormatAttribute : ValidationAttribute
    {
        private readonly string[] _formats;

        public AllowedFileFormatAttribute(params string[] formats)
        {
            _formats = formats;
        }

        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            if (value is IFormFile file)
            {
                var extension = Path.GetExtension(file.FileName);
                if (!_formats.Contains(extension.ToLower()))
                {
                    return new ValidationResult(ErrorMessage);
                }
            }
            else if (value is IEnumerable<IFormFile> files)
            {
                foreach (var fileItem in files)
                {
                    var extension = Path.GetExtension(fileItem.FileName);
                    if (!_formats.Contains(extension.ToLower()))
                    {
                        return new ValidationResult(ErrorMessage);
                    }
                }
            }
            return ValidationResult.Success;
        }

    }
}
