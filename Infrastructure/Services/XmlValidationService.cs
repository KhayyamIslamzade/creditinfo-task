﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace Infrastructure.Services
{

    public class XmlValidationService
    {
        private readonly IWebHostEnvironment _environment;
        private readonly FileService _fileService;
        private readonly string _xmlValidationDirectory;
        private readonly string _developmentDirectory;
        private readonly string _productionDirectory;
        private readonly bool _isProduction;

        public XmlValidationService(IWebHostEnvironment environment, FileService fileService)
        {
            _isProduction = environment.IsProduction();
            _environment = environment;
            _fileService = fileService;
            _developmentDirectory = environment.ContentRootPath;
            _productionDirectory = environment.ContentRootPath;
            _xmlValidationDirectory = Path.Combine(environment.ContentRootPath, "Files", "XmlValidations");

        }

        public void Validate(string xmlFileName, string xsdFileName)
        {
            var xmlPath = Path.Combine(_xmlValidationDirectory, xmlFileName);
            var xsdPath = Path.Combine(_xmlValidationDirectory, xsdFileName);

            XmlReaderSettings rs = new XmlReaderSettings();
            rs.Schemas.Add("", xsdPath);
            rs.ValidationType = ValidationType.Schema;
            rs.ValidationEventHandler += new ValidationEventHandler(ValidationEventHandler);


            XmlReader rd = XmlReader.Create(xmlPath, rs);

            while (rd.Read())
            {


            }
            Console.WriteLine("Done");
        }
        static void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Warning)
            {
                Console.Write("WARNING: ");
                Console.WriteLine(e.Message);
            }
            else if (e.Severity == XmlSeverityType.Error)
            {
                Console.Write("ERROR: ");
                Console.WriteLine(e.Message);
            }
        }

    }
}
