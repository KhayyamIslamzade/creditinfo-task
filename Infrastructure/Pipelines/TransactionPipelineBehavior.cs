﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Data;
using Infrastructure.Configurations;
using Infrastructure.Configurations.Commands;
using MediatR;

namespace Infrastructure.Pipelines
{
    public class TransactionPipelineBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : class, ICommand<TResponse>, ITransactionalRequest
    {
        private readonly ApplicationDbContext _dbContext;

        public TransactionPipelineBehavior(ApplicationDbContext dbContext
            )
        {
            _dbContext = dbContext ?? throw new ArgumentException(nameof(dbContext));
        }


        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken,
            RequestHandlerDelegate<TResponse> next)
        {
            try
            {

                var currentTransaction = _dbContext.Database.CurrentTransaction;
                var isFirstTransaction = currentTransaction == null;
                if (isFirstTransaction)
                {
                    Console.WriteLine($"Begin transaction {typeof(TRequest).Name}");
                    await _dbContext.Database.BeginTransactionAsync(cancellationToken);

                }

                var response = await next();

                if (isFirstTransaction)
                {
                    await _dbContext.Database.CommitTransactionAsync(cancellationToken);

                    Console.WriteLine($"Committed transaction {typeof(TRequest).Name}");
                }


                return response;
            }
            catch (Exception ex)
            {


                var isTransactionExist = _dbContext.Database.CurrentTransaction != null;

                if (isTransactionExist)
                {
                    Console.WriteLine($"Rollback transaction executed {typeof(TRequest).Name}");
                    await _dbContext.Database.RollbackTransactionAsync(cancellationToken);
                }


                throw;
            }
        }
    }
}
