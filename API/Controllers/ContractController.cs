﻿using System.Threading.Tasks;
using Application.Commands.ContractCommands.CreateContract;
using Application.Commands.ContractCommands.DeleteContract;
using Application.Commands.ContractCommands.UpdateContract;
using AutoWrapper.Extensions;
using AutoWrapper.Wrappers;
using Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class ContractController : BaseController
    {

        //[HttpGet("{id}")]
        //public async Task<ApiResponse> GetAsync(int id)
        //{
        //    var result = await Mediator.Send(new GetContractQuery(new GetContractRequest()
        //    {
        //        Id = id
        //    }));
        //    return new ApiResponse(result.Response);
        //}

        //[HttpGet]
        //public async Task<ApiResponse> GetAllAsync([FromQuery] ContractFilterParameters filterParameters, [FromQuery] PagingParameters pagingParameters, [FromQuery] SortParameters sortParameters)
        //{
        //    var result = await Mediator.Send(new GetAllContractQuery(new GetAllContractRequest()
        //    {
        //        FilterParameters = filterParameters,
        //        SortParameters = sortParameters,
        //        PagingParameters = pagingParameters
        //    }));

        //    return new ApiResponse(result.Response);
        //}

        [HttpPost]
        public async Task<ApiResponse> CreateAsync([FromBody] CreateContractRequest request)
        {
            if (!ModelState.IsValid)
                throw new ApiException(ModelState.AllErrors());

            var result = await Mediator.Send(new CreateContractCommand(request));
            return new ApiResponse(result: result.Response);
        }

        [HttpPut]
        public async Task<ApiResponse> UpdateAsync([FromBody] UpdateContractRequest request)
        {
            if (!ModelState.IsValid)
                throw new ApiException(ModelState.AllErrors());

            var result = await Mediator.Send(new UpdateContractCommand(request));
            return new ApiResponse(result: result.Response);
        }

        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteAsync(string id)
        {
            await Mediator.Send(new DeleteContractCommand(new DeleteContractRequest(id)));
            return new ApiResponse();
        }
    }
}
