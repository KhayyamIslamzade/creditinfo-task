﻿using System.Threading.Tasks;
using Application.Commands.SubjectRoleCommands.CreateSubjectRole;
using Application.Commands.SubjectRoleCommands.DeleteSubjectRole;
using Application.Queries.SubjectRoleQueries.Detail;
using Application.Queries.SubjectRoleQueries.Search;
using AutoWrapper.Extensions;
using AutoWrapper.Wrappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{

    public class SubjectRoleController : BaseController
    {


        [HttpGet("{nationalId}/search")]
        public async Task<ApiResponse> SearchAsync(int nationalId)
        {
            var result = await Mediator.Send(new SearchQuery(new()
            {
                NationalId = nationalId
            }));
            return new ApiResponse(result: result);
        }
        [HttpGet("{nationalId}/detail")]
        public async Task<ApiResponse> DetailAsync(int nationalId)
        {
            var result = await Mediator.Send(new DetailQuery(new()
            {
                NationalId = nationalId
            }));
            return new ApiResponse(result: result);
        }
        [HttpPost]
        public async Task<ApiResponse> CreateAsync([FromBody] CreateSubjectRoleRequest request)
        {
            if (!ModelState.IsValid)
                throw new ApiException(ModelState.AllErrors());

            var result = await Mediator.Send(new CreateSubjectRoleCommand(request));
            return new ApiResponse(result: result.Response);
        }

        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteAsync(int id)
        {
            await Mediator.Send(new DeleteSubjectRoleCommand(new DeleteSubjectRoleRequest(id)));
            return new ApiResponse();
        }
    }
}
