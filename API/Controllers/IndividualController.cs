﻿using System.Threading.Tasks;
using Application.Commands.IndividualCommands.CreateIndividual;
using Application.Commands.IndividualCommands.DeleteIndividual;
using Application.Commands.IndividualCommands.UpdateIndividual;
using AutoWrapper.Extensions;
using AutoWrapper.Wrappers;
using Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{

    public class IndividualController : BaseController
    {

        //[HttpGet("{id}")]
        //public async Task<ApiResponse> GetAsync(int id)
        //{
        //    var result = await Mediator.Send(new GetIndividualQuery(new GetIndividualRequest()
        //    {
        //        Id = id
        //    }));
        //    return new ApiResponse(result.Response);
        //}

        //[HttpGet]
        //public async Task<ApiResponse> GetAllAsync([FromQuery] IndividualFilterParameters filterParameters, [FromQuery] PagingParameters pagingParameters, [FromQuery] SortParameters sortParameters)
        //{
        //    var result = await Mediator.Send(new GetAllIndividualQuery(new GetAllIndividualRequest()
        //    {
        //        FilterParameters = filterParameters,
        //        SortParameters = sortParameters,
        //        PagingParameters = pagingParameters
        //    }));

        //    return new ApiResponse(result.Response);
        //}

        [HttpPost]
        public async Task<ApiResponse> CreateAsync([FromBody] CreateIndividualRequest request)
        {
            if (!ModelState.IsValid)
                throw new ApiException(ModelState.AllErrors());

            var result = await Mediator.Send(new CreateIndividualCommand(request));
            return new ApiResponse(result: result.Response);
        }

        [HttpPut]
        public async Task<ApiResponse> UpdateAsync([FromBody] UpdateIndividualRequest request)
        {
            if (!ModelState.IsValid)
                throw new ApiException(ModelState.AllErrors());

            var result = await Mediator.Send(new UpdateIndividualCommand(request));
            return new ApiResponse(result: result.Response);
        }

        [HttpDelete("{id}")]
        public async Task<ApiResponse> DeleteAsync(string id)
        {
            await Mediator.Send(new DeleteIndividualCommand(new DeleteIndividualRequest(id)));
            return new ApiResponse();
        }
    }
}
