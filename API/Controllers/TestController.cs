﻿using AutoWrapper.Wrappers;
using Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    public class TestController : BaseController
    {
        private readonly XmlValidationService _xmlValidationService;

        public TestController(XmlValidationService xmlValidationService)
        {
            _xmlValidationService = xmlValidationService;
        }
        [HttpGet]
        public ApiResponse Get()
        {
            _xmlValidationService.Validate("input.xml", "input.xsd");
            return new ApiResponse(new[] { "value 1", "value 2", "value 2" });
        }
    }
}
